To use this project do the following:

1: Run the following command
sudo apt install git && git clone https://gitlab.com/bart94122/defaultsetup.git && cd defaultsetup && chmod +x setup.sh && ./setup.sh

2: Add a line to the .bash_aliases, which calls the "projectSpaceAliases" command to create a workspace at the specified location:
projectSpaceAliases /home/me/example

3: Open that folder as project in IntelliJ

4: Place projects in the "projects" folder of that workspace