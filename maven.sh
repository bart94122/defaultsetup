MAVEN_VERSION="3.9.1"
MAVEN_INSTALL="/usr/share/maven"

wget "https://dlcdn.apache.org/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz"
tar xzf "apache-maven-${MAVEN_VERSION}-bin.tar.gz"
pushd "${MAVEN_INSTALL}" > /dev/null
sudo rm -rf bin
sudo rm -rf boot
sudo rm -rf lib
sudo rm -rf man
popd > /dev/null
pushd "apache-maven-${MAVEN_VERSION}" > /dev/null
sudo mv bin "${MAVEN_INSTALL}"
sudo mv boot "${MAVEN_INSTALL}"
sudo mv lib "${MAVEN_INSTALL}"
popd > /dev/null
rm -rf "apache-maven-${MAVEN_VERSION}"
rm -rf "apache-maven-${MAVEN_VERSION}-bin.tar.gz"
echo -n "Installed Maven version: " && mvn -version