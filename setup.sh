#!/bin/bash

cp bash_aliases ../.bash_aliases

sudo apt install terminator maven openjdk-17-jdk openjdk-17-source gnome-session-flashback gimp numlockx
sudo apt remove gnome-sudoku aisleriot gnome-mahjongg gnome-mines simple-scan shotwell transmission-common cheese rhythmbox
sudo apt autoremove
sudo apt update && sudo apt upgrade && sudo apt autoremove && sudo apt autopurge
sudo snap install intellij-idea-community --classic

source ./maven.sh