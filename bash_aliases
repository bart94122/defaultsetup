function projectSpaceAliases() {
	local workspace=$1
	local workspaceData="$workspace/data"
	local workspaceLogs="$workspace/logs"
	local workspaceProjects="$workspace/projects"
	local workspaceVars="$workspace/vars"

	mkdir -p ${workspaceLogs} ${workspaceData} ${workspaceProjects} ${workspaceVars}
	alias cd$(basename $workspace)="cd ${workspace}"
	alias cd$(basename $workspace)logs="cd ${workspaceLogs}"
	alias cd$(basename $workspace)data="cd ${workspaceData}"
	alias cd$(basename $workspace)vars="cd ${workspaceVars}"

	local array=($(ls "${workspaceProjects}"))
	for i in "${array[@]}"; do
	    s="${i/-/''}"
	    alias cd$(basename $workspace)${s,,}="cd ${workspaceProjects}/$i"
	done

}

function localmvn() {
	ACTION="build"
	mvn clean install $* | tee ../../logs/$(basename $(pwd))-${ACTION}.log.tmp
	cat ../../logs/$(basename $(pwd))-${ACTION}.log.tmp | ansi2txt > ../../logs/$(basename $(pwd))-${ACTION}.log
	rm ../../logs/$(basename $(pwd))-${ACTION}.log.tmp
}

function localtree() {
	ACTION="tree"
	mvn dependency:tree $* | tee ../../logs/$(basename $(pwd))-${ACTION}.log.tmp
	cat ../../logs/$(basename $(pwd))-${ACTION}.log.tmp | ansi2txt > ../../logs/$(basename $(pwd))-${ACTION}.log
	rm ../../logs/$(basename $(pwd))-${ACTION}.log.tmp
}

function localeffpom() {
	mvn help:effective-pom -D verbose=true -D output=../../logs/$(basename $(pwd))-effpom.xml
}

function localcargo() {
	ACTION="run"
	mvn -P cargo.run $* | tee ../../logs/$(basename $(pwd))-${ACTION}.log.tmp
	cat ../../logs/$(basename $(pwd))-${ACTION}.log.tmp | ansi2txt > ../../logs/$(basename $(pwd))-${ACTION}.log
	rm ../../logs/$(basename $(pwd))-${ACTION}.log.tmp
}

function localboot() {
	ACTION="run"
	mvn spring-boot:run $* | tee ../../logs/$(basename $(pwd))-${ACTION}.log.tmp
	cat ../../logs/$(basename $(pwd))-${ACTION}.log.tmp | ansi2txt > ../../logs/$(basename $(pwd))-${ACTION}.log
	rm ../../logs/$(basename $(pwd))-${ACTION}.log.tmp
}

alias localwatchlogs="watch 'grep -r -E \"FAILURE|Skipped: [1-9]\" *.log.tmp'"
alias localclearwatchlogs="rm *.log && localwatchlogs"
alias gpnoci="git push -o ci.skip"
alias gp="git push"
alias gs="git status"

numlockx on